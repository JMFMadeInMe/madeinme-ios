//
//  JMFDesignViewController.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMFCollectionView.h"
#import "MenuPath.h"

@interface JMFDesignViewController : UIViewController <JMFCollectionViewDelegate> {
    UIButton* button1;
    UIButton* button2;
    UIButton* button3;
    UIButton* button4;
    UIButton* button5;
    UIButton* main;
    MenuPath* navigation;
    
}

@property (retain) MenuPath* navigation;


/*
 * Paleta: Tipo, Subpartes y Meteriales (Collection view)
 */
@property (weak, nonatomic) IBOutlet UILabel *lblTypes;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTypesOfShoePart;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeSubparts;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeMaterials;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewMyMaterials;
@property (weak, nonatomic) IBOutlet UIView *linePaletteTypes;

- (IBAction)onClickItemChangedShoepartsSegmentControl:(id)sender;

// Zapato
@property (weak, nonatomic) IBOutlet UIImageView *shoeImage;
- (IBAction)onShoePanGestures:(id)sender;
- (IBAction)onShoeTapGestures:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *selectorImage;
@property (weak, nonatomic) IBOutlet UIImageView *tagToecap;

// Partes (Segment)
@property (weak, nonatomic) IBOutlet UISegmentedControl *shoePartsSegmentControl;

// Adornos y Extras (Menu Path)
@property (nonatomic, retain) IBOutlet UIButton *button1;
@property (nonatomic, retain) IBOutlet UIButton *button2;
@property (nonatomic, retain) IBOutlet UIButton *button3;
@property (nonatomic, retain) IBOutlet UIButton *button4;
@property (nonatomic, retain) IBOutlet UIButton *button5;
@property (nonatomic, retain) IBOutlet UIButton *main;

- (IBAction) onClickItemChangedMenuPath:(id)sender;

// Estado de la configuración
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;


// Precio, Compra y Detalles
- (IBAction)onClickDetailsButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *showDetailsButton;
@property (weak, nonatomic) IBOutlet UIImageView *detailsImageView;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (weak, nonatomic) IBOutlet UILabel *detailsDummyLabel;

- (IBAction)onClickBuyButton:(id)sender;






-(NSString *) getShoepartTitle:(int) shoepartIndex;

@end
