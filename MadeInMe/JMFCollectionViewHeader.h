//
//  JMFCollectionViewHeader.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 15/09/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFCollectionViewHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
