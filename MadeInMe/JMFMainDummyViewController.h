//
//  JMFMainViewController.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 24/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFMainDummyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgLaspcape;
@property (weak, nonatomic) IBOutlet UILabel *mainDummyLabel;

- (IBAction)btnStartDesignScreenLasdcape:(id)sender;
- (IBAction)btnSelectStyle:(id)sender;


@end
