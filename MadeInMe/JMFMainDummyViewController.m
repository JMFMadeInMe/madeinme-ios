//
//  JMFMainViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 24/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFMainDummyViewController.h"
#import "JMFDesignViewController.h"
#import "Utils.h"
#import "Colors.h"



@interface JMFMainDummyViewController ()

@end

@implementation UINavigationController (Rotation_IOS6)



-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    
    return UIInterfaceOrientationPortrait;
    
}

@end

@implementation JMFMainDummyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [Utils colorWithHexString:COLOR_MadeInMegrisDark];

    self.mainDummyLabel.transform = CGAffineTransformMakeRotation(3.14/-10.5);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma - Actions
- (IBAction)btnStartDesignScreenPostrait:(id)sender {
    [self startDesign];

}

- (IBAction)btnStartDesignScreenLasdcape:(id)sender {
    [self startDesign];
}

- (IBAction)btnSelectStyle:(id)sender {
    [self startDesign];
}

-(void) startDesign {
    
    JMFDesignViewController *designVC = [JMFDesignViewController new];
//    JMFViewController *designVC = [JMFViewController new];
    [self.navigationController pushViewController:designVC animated:YES];
    
}


@end
