//
//  Model.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//
/* ......................................................................................
 * **************************
 * ** Modelo para pruebas  **
 * **************************
 * 
 * El modelo real debe incorporar los siguientes métodos:
 
 * [self.model count]                      --> número de imagenes.
 * [self.model countSections]              --> número de secciones.
 * [self.model image:indexPath]            --> imagen del item.
 * [self.model key:indexPath]              --> 'key' asociada a cada item. Se manda al protocolo para saber identificar el item seleccionado. De esta manera se puede controlar desde el modelo como se identificará cada item idependientemente del orden en que esten presentados en la collectionView. Esta 'key' puede ser cualquier cadena que el siguiente modelo sabrá reconocer para cargar la nueva collectionView.
 
 *                                          model1->key1
 *        (model1)collectionView1(tipos) -------------->
 *                                                 model2->key2
 *    --> (model2:key1)collectionView2(opciones) -------------->
 *    --> (model3:key2)collectionView3(materials)
 *
 ......................................................................................*/
#import "ModelDummy.h"
#import "Utils.h"

@interface ModelDummy()

//@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) NSDictionary *dataDictionary;


@end

@implementation ModelDummy


#pragma mark - Init (el modelo real los puede incorporar o crear modelos dieferentes para cada collectionView)
-(id) initWithTypesOfShoePart:(NSString *)shoePart {
    if (self = [super init]) {
        
        self.key = [[NSString alloc] initWithFormat:@"%@_subpart",shoePart]; // @"trasera_subpart"
        NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
        NSArray *dataArrayTemp = [self loadImages:[[NSString alloc] initWithFormat:@"%@_type",shoePart]];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:shoePart];
        
        _dataDictionary = dataDictionaryTemp;

        //        _dataArray = [self loadImages:[[NSString alloc] initWithFormat:@"%@_type",shoePart]];
    }
    return self;
}

-(id) initWithShoeSubparts:(NSString *)shoeSubparts {
    if (self = [super init]) {
        
        self.key = @"color";
        NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
        NSArray *dataArrayTemp = [self loadImages:shoeSubparts];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:shoeSubparts];
        
        _dataDictionary = dataDictionaryTemp;
 
//        _dataArray = [self loadImages:shoeSubparts];
    }
    return self;
}

-(id) initWithMaterials:(NSString *)key {
    if (self = [super init]) {
        
//        _dataArray = [self loadImages:key];
        
        NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
        NSArray *dataArrayTemp = [self loadImages:@"color_ante"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"ante"];
        dataArrayTemp = [self loadImages:@"color_charol"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"charol"];
        
        _dataDictionary = dataDictionaryTemp;
    }
    return self;
}



//-(id) initWithMaterials:(NSString *)shoePart shoesubpart:(NSString *)shoeSubpart {
//    if (self = [super init]) {
//        
//        [self loadMaterials];
//
//        //        [self newGameWithLevel:_level];
//    }
//    return self;
//}
//
//-(void)loadMaterials {
//    
//    NSArray *dataArrayTemp = [self loadImages:@"color_ante"];
//    [_dataDictionary setValue:dataArrayTemp forKey:@"ante"];
//    dataArrayTemp = [self loadImages:@"color_charol"];
//    [_dataDictionary setValue:dataArrayTemp forKey:@"charol"];
//}

#pragma mark - Métodos principales (los que necesita la clase JMFColletionView)

-(NSInteger) count:(NSInteger)section {
    
    NSArray *keys = [self.dataDictionary allKeys];
    id aKey = [keys objectAtIndex:section];
    return [[self.dataDictionary objectForKey:aKey] count];

    
//    
//    if ([self.dataArray count] > 0)
//        return [self.dataArray count];
//    else     if ([self.dataDictionary count] > 0)
//        return [[self.dataDictionary objectForKey:@"ante" ] count];
//    else
//        return 0;

}

-(NSInteger) countSections {
    NSInteger numSections = [[self.dataDictionary allKeys] count];
    if (numSections == 0)
        numSections = 1;
    
    return numSections;
}

-(UIImage *) image:(NSIndexPath *)indexPath{
    
    
    NSArray *dataArrayTemp = [NSArray new];
    NSArray *keys = [self.dataDictionary allKeys];
    id aKey = [keys objectAtIndex:indexPath.section];
    dataArrayTemp = [self.dataDictionary objectForKey:aKey];

    
//    //    return [UIImage imageNamed:@"puntera__depeep.png"];
//    NSArray *dataArrayTemp = [NSArray new];
//    if ([self.dataDictionary count] > 0) {
//        
//        NSArray *keys = [self.dataDictionary allKeys];
//        id aKey = [keys objectAtIndex:indexPath.section];
//        dataArrayTemp = [self.dataDictionary objectForKey:aKey];
//        
////        dataArrayTemp = [self.dataDictionary objectForKey:@"ante"];
//    }
//    else
//        dataArrayTemp = self.dataArray;
    
    return [UIImage imageNamed:[dataArrayTemp objectAtIndex:indexPath.row]];
    
}

/*........................................................................................
*
*    'key' asociada a cada item. Se manda al protocolo para saber identificar el
* item seleccionado. De esta manera se puede controlar desde el modelo como se
* identificará cada item idependientemente del orden en que esten presentados en
* la collectionView. Esta 'key' puede ser cualquier cadena que el siguiente modelo 
* sabrá reconocer para cargar la nueva collectionView.
*
*                                          model1->key1
*        (model1)collectionView1(tipos) -------------->
*                                                 model2->key2
*    --> (model2:key1)collectionView2(opciones) --------------> 
*    --> (model3:key2)collectionView3(materials)
 
..........................................................................................*/
-(NSString *)key:(NSIndexPath *)indexPath {
    if ([self.key isEqualToString:@"color"])
        return self.key;
    else
        return [[NSString alloc] initWithFormat:@"%@%u", self.key, indexPath.row ];
}




#pragma mark - Varios
-(NSInteger) countTypesOfPartShoe:(int)shoePart {
    return 20;
}
//-(UIImage *) loadTypesOfShoePart:(int)shoePart {
//
//    return [UIImage imageNamed:@"puntera__depeep.png"];
//}

-(NSArray *) loadTypesOfShoePart:(int)shoePart {
    
    return (NSArray *)[UIImage imageNamed:@"puntera__depeep.png"];
}


-(NSArray *)loadImages:(NSString *)filesPrefix {
    NSMutableArray *result = [NSMutableArray array];
    
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"png"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"jpg"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"jpeg"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"gif"]];
    
    return result;
}

-(NSArray *)loadData:(NSString *)filesPrefix {
    NSMutableArray *result = [NSMutableArray array];
    
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"png"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"jpg"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"jpeg"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"gif"]];
    
    return result;
}


@end
