//
//  Model.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelDummy : NSObject

@property (strong, nonatomic) NSMutableArray *arrayOfAreasShoe;
@property (strong, nonatomic) NSMutableArray *arrayOfParts;
@property (strong, nonatomic) NSMutableArray *arrayOfMaterials;
@property int shoePartActual;





// init (el modelo real los puede incorporar o crear modelos dieferentes para cada collectionView)
-(id) initWithTypesOfShoePart:(NSString *)shoePart;
-(id) initWithShoeSubparts:(NSString *)typeOfShoePart;
-(id) initWithMaterials:(NSString *)shoePart shoesubpart:(NSString *)shoeSubpart;
-(id) initWithMaterials:(NSString *)key;

// Métodos principales (los que necesita la clase JMFColletionView)
-(NSInteger) count:(NSInteger) section;
-(NSInteger) countSections;
-(UIImage *) image:(NSIndexPath *)indexPath;
-(NSString *)key:(NSIndexPath *)indexPath;

// Varios
-(NSInteger) countTypesOfPartShoe:(int)shoePart;
-(NSArray *) loadTypesOfShoePart:(int)shoePart;
-(NSArray *)loadImages:(NSString *)filesPrefix;
-(NSDictionary *)loadData:(NSString *)filesPrefix;



@end
