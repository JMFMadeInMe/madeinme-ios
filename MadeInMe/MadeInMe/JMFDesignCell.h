//
//  JMFDesignCell.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFDesignCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
