//
//  ViewController.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 28/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol JMFCollectionViewDelegate

-(void)collectionViewChanged:(NSString *)nameCallback key:(NSString *)key indexPath:(NSIndexPath *)indexPath;

@end

@interface JMFCollectionView : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    id delegate;
}

@property (nonatomic, retain) id<JMFCollectionViewDelegate> delegate;

-(id) initWithCollectionView:(UICollectionView *) collectionView
                       model:(id)model
          setScrollDirection:(UICollectionViewScrollDirection) scrollDirection
                    delegate:(id)delegate
                nameCallback:(NSString *)nameCallback;

@end
