//
//  Colors.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 31/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "Colors.h"

NSString *const COLOR_MadeInMe           = @"#1AC2B0";
NSString *const COLOR_MadeInMeDarck      = @"#2DAC9E";
NSString *const COLOR_MadeInMeLight      = @"#48CEC0";

NSString *const COLOR_MadeInMegrisDark   = @"#7E838C";
NSString *const COLOR_MadeInMegrisLight  = @"#F5F5F5";

@implementation Colors


@end
