//
//  JMFDesignViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//
/* ....................................................................................
 *
 *  ====================
 *         VISTA
 *  ====================
 *
 * Es el motor para diseñar un zapato. Mediante views (segment, collectionView,...) se accede a las diferentes opciones para configurar el zapato.
 *
 *  ______________________________________________________________
 *  |
 *  | -----------------------
 *  || SEGMENT CTRL (Partes) |<- Alternativa I
 *  | -----------------------
 *  | --  -------------
 *  ||  ||             -> CollectionView2 (Opciones de un tipo)
 *  ||  | ------------
 *  ||  | # # #
 *  ||<- CollectionnView1 (Tipos de una parte)
 *  ||  |
 *  ||  | # # # # # #
 *  ||  | # # # # # # -> CollectionView3
 *  ||  | # # # # # #      (Materiales)
 *  | --          ----------------------------------------------
 *  |            | Alternativa II para el segmento ctl (Partes) |
 *  ________________________________________________________________
 
 *
 *
 *
 *  - SegmentControl: (Partes del zapato)
 *          @property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlPartsShoe;
 *
 *          -> Presenta las PARTES del zapato a selecionar:Cuerpo,Puntera,Trasera,...
 *
 *  - CollectionView1: (Tipos de una parte)
 *          @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTypesOfShoePart;
 *
 *          -> Muestra los diferentes TIPOS de una PARTE del zapato (diferentes punteras por ejemplo), tras seleccionar una parte del zapato.
 *
 *  - CollectionView2: (Opciones para un tipo)
 *          @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeSubparts;
 *
 *          -> OPCIONES de un TIPO.
 *
 *  -CollectionView3: (Materiales de la opción)
 *          @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewMaterials;
 *
 *          -> MATERIALES asociados a la PARTE, TIPO y OPCIÓN elegidos
 *
 *
 *
 *  ============================
 *    Clases y Métodos llamados
 *  ============================
 *
 *  Se crean tres collectionView por medio de la clase JMFCollectionView:
 *
 *      @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTypesOfShoePart;
 *      @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeSubparts;
 *      @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewMaterials;
 *
 *
 *
 *  Para mostrar los diferentes collectionView se utilizan los métodos:
 *
 *                  -(void)showTypesOfShoePart:(int)key
 *                  -(void)showShoeSubparts:(NSString *)key
 *                  -(void)showMaterials:(NSString *)key
 *
 *
 *
 *
 *
 *
 *  ====================
 *    JMFCollectionView
 *  ====================
 *
 *
 *          [[JMFCollectionView alloc]
 *  (UICollectionView *) initWithCollectionView:self.collectionViewMaterials
 *                                        model:(id)model
 *                           setScrollDirection:(UICollectionViewScrollDirection) scrollDirection
 *                                      delegate:(id)aDelegate
 *                                  nameCallback:(NSString *)nameCallback
 *
 *        -------------
 *          Protocolo:
 *        -------------
 *                    -(void)collectionViewChanged:(NSString *)nameCallback
 *                                   key:(NSString *)key
 *                                   indexPath:(NSIndexPath *)indexPath
 *
 *              ---------------------------------------------------------------------
 *          -> Cada collectionView se gestiona desde la esta clase, que recibe un 
 *              nameCallback:(NSString *)nameCallback y un modelo (model:(id)model).
 *              ---------------------------------------------------------------------
 *
 *              El nameCallback es una identificador con el que se le reconnoce en el protocolo 'collectionViewChanged'.
 *              EL modelo le proporciona los datos para el uso en el collectionView. Debe contener los siguientes metodos:
 
                                       * [self.model count]                      --> número de imagenes.
                                       * [self.model countSections]              --> número de secciones.
                                       * [self.model image:indexPath]            --> imagen del item.
                                       * [self.model key:indexPath]              --> 'key' asociada a cada item. Se manda al protocolo para saber identificar el item seleccionado. De esta manera se puede controlar desde el modelo como se identificará cada item idependientemente del orden en que esten presentados en la collectionView. Esta 'key' puede ser cualquier cadena que el siguiente modelo sabrá reconocer para cargar la nueva collectionView.
 
                                     model1->key1                                          model2->key2
   (model1)collectionView1(tipos) --------------> (model2:key1)collectionView2(opciones) --------------> (model3:key2)collectionView3(materials)
 
 
 *
 .........................................................................................*/
#import <QuartzCore/QuartzCore.h>

#import "JMFDesignViewController.h"
#import "ModelDummy.h"
#import "JMFDesignParts.h"
#import "JMFDesignCell.h"
#import "JMFCollectionView.h"

#import "ExpandableNavigation.h"
#import "Utils.h"
#import "Colors.h"

// Pestañas zapato.
static int const SHOE_PART_CUERPO    = 0;
static int const SHOE_PART_PUNTERA   = 1;
static int const SHOE_PART_TRASERA   = 2;
static int const SHOE_PART_TACON     = 3;
static int const SHOE_PART_ADRONOS   = 4;
static int const SHOE_PART_EXTRAS    = 6;

static NSString * const SHOE_PARTS_ARRAY[] = {  @"cuerpo",
                                                @"puntera",
                                                @"trasera",
                                                @"tacon",
                                                @"adornos",
                                                @"extras"};
static NSString * const SHOE_PARTS_ARRAY_TITLE_TYPES[] = {  @"Cuerpos",
    @"Punteras",
    @"Traseras",
    @"Tacones",
    @"Adornos",
    @"Extras"};


static NSString *const NAMECALLBACK_TYPE_OF_SHOE_PART = @"viewTypeOfShoePart";
static NSString *const NAMECALLBACK_SHOE_SUBPART = @"viewShoeSubpart";
static NSString *const NAMECALLBACK_MATERIALS = @"viewMaterials";

@interface JMFDesignViewController ()

@property(nonatomic,strong) ModelDummy *model;

@property(nonatomic, strong) JMFCollectionView *jmfCollectionViewTypesOfShoePart;
@property(nonatomic, strong) JMFCollectionView *jmfCollectionViewShoeSubparts;
@property(nonatomic, strong) JMFCollectionView *jmfCollectionViewMaterials;
//@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation JMFDesignViewController

@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;
@synthesize button5;
@synthesize main;
@synthesize navigation;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /* ---------------------
     * setup CollectionView
      ---------------------*/
    // Llamada inicial con puntera:'SHOE_PARTS_ARRAY[SHOE_PART_CUERPO]'
    [self showTypesOfShoePart:SHOE_PART_CUERPO];
    
    /* -------------------
     * Menu path animation
      --------------------*/
    // initialize ExpandableNavigation object with an array of buttons.
    NSArray* buttons = [NSArray arrayWithObjects:button1, button2, button3, button4, button5, nil];
    self.navigation = [[ExpandableNavigation alloc] initWithMenuItems:buttons mainButton:self.main radius:120.0];
    
    /* -------------
     * Colores view
      --------------*/
    self.lblTypes.tintColor = [Utils colorWithHexString:COLOR_MadeInMe];
    self.segmentControlPartsShoe.tintColor = [Utils colorWithHexString:COLOR_MadeInMe];
        
    self.collectionViewTypesOfShoePart.layer.borderColor = [Utils colorWithHexString:COLOR_MadeInMe].CGColor;
    self.collectionViewTypesOfShoePart.layer.borderWidth = 1;
    
    self.collectionViewMaterials.layer.backgroundColor = [Utils colorWithHexString:COLOR_MadeInMegrisLight].CGColor;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.button1 = nil;
    self.button2 = nil;
    self.button3 = nil;
    self.button4 = nil;
    self.button5 = nil;
    self.main = nil;
    self.navigation = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Protocolos

- (IBAction)segmentedSectionsShoes:(id)sender {
}

/** ...................................
 *
 *   Selección de Segement ShoeParts
 *
 ......................................*/
- (IBAction)segmentControlShoePartChanged:(id)sender {
    
    [self showTypesOfShoePart:self.segmentControlPartsShoe.selectedSegmentIndex];
}

/** .................................
 *
 *   Selección de JMFCollectionView
 *
 ....................................*/
-(void)collectionViewChanged:(NSString *)nameCallback
                key:(NSString *)key
                   indexPath:(NSIndexPath *)indexPath {
    
    /* ----------------------------------
     * collectionView para las subpartes
     ------------------------------------*/
    if (nameCallback == NAMECALLBACK_TYPE_OF_SHOE_PART) {
        
        [self showShoeSubparts:key];


        
        
   /* ----------------------------------
    * collectionView para los materiales
    ------------------------------------*/
    } else if (nameCallback == NAMECALLBACK_SHOE_SUBPART) {
        [self showMaterials:key];
    }
    
    
}


- (IBAction) touchMenuItem:(id)sender {
    
    // if the menu is expanded, then collapse it when an menu item is touched.
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:nil
                                                       message:[(UIButton *)sender currentTitle]
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] ;
    [message show];
    
    if( self.navigation.expanded ) {
        [self.navigation collapse];
    }
}


#pragma mark - Metodos privados

-(void)showTypesOfShoePart:(int)key {
    
    self.jmfCollectionViewTypesOfShoePart = [[JMFCollectionView alloc]
                                             initWithCollectionView:self.collectionViewTypesOfShoePart
                                             model:[[ModelDummy alloc] initWithTypesOfShoePart:SHOE_PARTS_ARRAY[key]]
                                             setScrollDirection:UICollectionViewScrollDirectionVertical
                                             delegate:self
                                             nameCallback:NAMECALLBACK_TYPE_OF_SHOE_PART];
    // Titulo
    self.lblTypes.text = SHOE_PARTS_ARRAY_TITLE_TYPES[key];
    
    // Vacia collectionView
    [self showShoeSubparts:@""];
//    self.collectionViewShoeSubparts.backgroundColor = [UIColor clearColor];
}

-(void)showShoeSubparts:(NSString *)key {
    
    self.jmfCollectionViewShoeSubparts = [[JMFCollectionView alloc]
                                          initWithCollectionView:self.collectionViewShoeSubparts
                                          model:[[ModelDummy alloc] initWithShoeSubparts:key]
                                          setScrollDirection:UICollectionViewScrollDirectionHorizontal
                                          delegate:self
                                          nameCallback:NAMECALLBACK_SHOE_SUBPART];
    //        self.jmfCollectionViewShoeSubparts.view.layer.borderColor =[UIColor redColor].CGColor;
    //        self.jmfCollectionViewShoeSubparts.view.layer.borderWidth = 3.0f;

    // Vacia collectionView
    [self showMaterials:@""];
//    self.collectionViewMaterials.backgroundColor = [UIColor clearColor];

}

-(void)showMaterials:(NSString *)key {
    
    self.jmfCollectionViewMaterials = [[JMFCollectionView alloc]
                                       initWithCollectionView:self.collectionViewMaterials
                                       model:[[ModelDummy alloc] initWithMaterials:key]
                                       setScrollDirection:UICollectionViewScrollDirectionVertical
                                       delegate:self
                                       nameCallback:NAMECALLBACK_MATERIALS];
}


@end



