//
//  Utils.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 28/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Utils : NSObject

+(NSArray *)loadImagesFromAssets:(NSString *)filesPrefix fileType:(NSString *)fileType;
//+(NSArray *)loadImagesAndKeyFromAssets:(NSString *)filesPrefix fileType:(NSString *)fileType;
+(UIColor*)colorWithHexString:(NSString*)hex;

@end
