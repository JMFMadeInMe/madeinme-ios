//
//  Colors.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 31/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <Foundation/Foundation.h>

// Colores
extern NSString *const COLOR_MadeInMe;
extern NSString *const COLOR_MadeInMeDarck;
extern NSString *const COLOR_MadeInMeLight;

extern NSString *const COLOR_MadeInMegrisDark;
extern NSString *const COLOR_MadeInMegrisLight;




@interface Colors : NSObject


@end
