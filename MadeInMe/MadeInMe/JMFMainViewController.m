//
//  JMFMainViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 24/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFMainViewController.h"
#import "JMFDesignViewController.h"
#import "Utils.h"
#import "Colors.h"



@interface JMFMainViewController ()

@end

@implementation UINavigationController (Rotation_IOS6)



-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
    
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    
    return UIInterfaceOrientationPortrait;
    
}

@end

@implementation JMFMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setBackgroundColor:[UIColor greenColor]];
    
//    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];

//self.navigationController.navigationBar.barTintColor = [self colorWithHexString:@"FF0000"];
//    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"MadeInMe - Cabecera.jpg"]
//                                       forBarMetrics:UIBarMetricsDefault];
    
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];

 
    self.navigationController.navigationBar.tintColor = [Utils colorWithHexString:COLOR_MadeInMe];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [Utils colorWithHexString:COLOR_MadeInMeDarck];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma - Actions
- (IBAction)btnStartDesignScreenPostrait:(id)sender {
    [self startDesign];

}

- (IBAction)btnStartDesignScreenLasdcape:(id)sender {
    [self startDesign];
}

-(void) startDesign {
    
    JMFDesignViewController *designVC = [JMFDesignViewController new];
//    JMFViewController *designVC = [JMFViewController new];
    [self.navigationController pushViewController:designVC animated:YES];
    
}


@end
