//
//  ViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 28/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//
/* .......................................................................................
 *
 *  ====================
 *    JMFCollectionView
 *  ====================
 *
 *
 *          [[JMFCollectionView alloc]
 *  (UICollectionView *) initWithCollectionView:self.collectionViewMaterials
 *                                        model:(id)model
 *                           setScrollDirection:(UICollectionViewScrollDirection) scrollDirection
 *                                      delegate:(id)aDelegate
 *                                  nameCallback:(NSString *)nameCallback
 *
 *        -------------
 *          Protocolo:
 *        -------------
 *                    -(void)collectionViewChanged:(NSString *)nameCallback
 *                                   key:(NSString *)key
 *                                   indexPath:(NSIndexPath *)indexPath
 *
 *              ---------------------------------------------------------------------
 *          -> Cada collectionView se gestiona desde la esta clase, que recibe un
 *              nameCallback:(NSString *)nameCallback y un modelo (model:(id)model).
 *              ---------------------------------------------------------------------
 *
 *              El nameCallback es una identificador con el que se le reconnoce en el protocolo 'collectionViewChanged'.
 *              EL modelo le proporciona los datos para el uso en el collectionView. Debe contener los siguientes metodos:
 
 * [self.model count]                      --> número de imagenes.
 * [self.model countSections]              --> número de secciones.
 * [self.model image:indexPath]            --> imagen del item.
 * [self.model key:indexPath]              --> 'key' asociada a cada item. Se manda al protocolo para saber identificar el item seleccionado. De esta manera se puede controlar desde el modelo como se identificará cada item idependientemente del orden en que esten presentados en la collectionView. Esta 'key' puede ser cualquier cadena que el siguiente modelo sabrá reconocer para cargar la nueva collectionView.
 
 model1->key1                                          model2->key2
 (model1)collectionView1(tipos) --------------> (model2:key1)collectionView2(opciones) --------------> (model3:key2)collectionView3(materials)
 
 
 *
 .........................................................................................*/

#import "JMFCollectionView.h"
#import "JMFDesignCell.h"
#import "JMFCollectionReusableView.h"
#import "ModelDummy.h"

static NSString *const CELL_IDENTIFIER = @"CellView";
static NSString *const HEADER_IDENTIFIER = @"HeaderView";

@interface JMFCollectionView ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) ModelDummy *model;
@property (nonatomic, strong) NSArray *dataArray;
@property int shoeType;
@property (nonatomic,strong) NSString *mNameCallback;

@end

@implementation JMFCollectionView

-(id) initWithCollectionView:(UICollectionView *) collectionView model:(id)model
          setScrollDirection:(UICollectionViewScrollDirection) scrollDirection
                    delegate:(id)aDelegate
                nameCallback:(NSString *)nameCallback
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _model = model; //[Model new];
        _collectionView = collectionView;
        
        /* ---------------------
         * setup CollectionView
         ----------------------*/
        [self.collectionView registerClass:[JMFDesignCell class] forCellWithReuseIdentifier:CELL_IDENTIFIER];
        [self.collectionView registerClass:[JMFCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER];

        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        
//        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
        [flowLayout setScrollDirection:scrollDirection];
        flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 20, 10);
        flowLayout.headerReferenceSize = CGSizeMake(self.collectionView.frame.size.width, 10.f);
//        flowLayout.minimumInteritemSpacing = 50;
//        flowLayout.minimumLineSpacing = 50;
        [self.collectionView setCollectionViewLayout:flowLayout];
        
//        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
//        flowLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0);
        
        
        /* --------
         * Delegado
         ----------*/
        _delegate = aDelegate;
        _mNameCallback = nameCallback;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource


- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [self.model count:section];
    //    return [self.dataArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return [self.model countSections];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JMFDesignCell *cell = [cv dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    //    cell.coverImageView.image = [UIImage imageNamed:@"puntera__depeep.png"];  //[self.model zonesShoes:@"Puntera"];
    
    //    cell.img.image = [self.model loadTypesOfShoePart:1]; //[UIImage imageNamed:@"puntera__depeep.png"];
    //    cell.img.image = [self.dataArray objectAtIndex:indexPath.row]; //[UIImage imageNamed:@"puntera__depeep.png"];
    
    cell.img.image = [self.model image:indexPath]; //[UIImage
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    [self.delegate TypeOfShoePartChanged:self.mNameCallback shoeSubparts:@"trasera_subpart" indexPath:indexPath];
    [self.delegate collectionViewChanged:self.mNameCallback key:[self.model key:indexPath] indexPath:indexPath];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    JMFDesignCell *cell = [JMFDesignCell new];
    
    // Se ajusta al tamaño de la celda del 'XIB'.
    CGSize cellSize = CGSizeMake(cell.bounds.size.width, cell.bounds.size.height);
    
    return cellSize;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

/*
 // Espaciado entre las celdas, las cabeceras y pies de página
 - (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout*)collectionViewLayout
 insetForSectionAtIndex:(NSInteger)section {
 
 return UIEdgeInsetsMake(50, 20, 50, 20);
 }
*/

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {

    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        JMFCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER forIndexPath:indexPath];
        NSString *title = [[NSString alloc]initWithFormat:@"Recipe Group #%i", indexPath.section + 1];
        headerView.title.text = title;
        
        reusableview = headerView;
    }
    
    
    return reusableview;
}



@end
