//
//  JMFSignupDummyViewController.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 22/09/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFSignupDummyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *signupDummyLabel;

@end
