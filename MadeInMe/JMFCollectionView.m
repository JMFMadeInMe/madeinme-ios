//
//  ViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 28/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//
/* ....................................................................................................
 *
 * Controla una collection view con una collectionview (vista a rellenar), un modelo, directionScroll, delegado (->ItemChanged), una etiqueta (título) y elemento actual (item check).
 *
 *  ------------
 *   Protocolos
 *  ------------
 *          -> -(void)onClickItemChangedCollectionViewWithTag:(NSString *)tag itemIndexPath:(NSIndexPath *)itemIndexPath;
 *
 *                 * tag:           identifica el collection view que envia el mensaje.
 *                 * itemIndexPath: section y row
 *
 *          -> -(NSString *)onGetSectionNameWithTag:(int)section tag:(NSString *)tag;
 *
 *  ------------
 *   Parámetros
 *  ------------
 *  initWithCollectionView:(UICollectionView *) collectionView:     --> Vista del collectionview.
 *  model:(id)model:                                                --> Modelo (métodos: countItems, countSections, image).
 *  setScrollDirection:(UICollectionViewScrollDirection) scrollDirection  --> ScrollDirection.
 *  delegate:(id)aDelegate                                          --> Delegado.
 *  tag:(NSString *)tag                                             --> Etiqueta (title).
 *  itemIndexPathCurrent:(NSIndexPath *)itemIndexPathCurrent        --> Elemento actual.
 *
 ....................................................................................................*/


#import "JMFCollectionView.h"
#import "JMFCollectionViewCell.h"

#import "JMFCollectionViewHeader.h"

#import "ModelDummy.h"
#import "Utils.h"
#import "Colors.h"

static NSString *const CELL_IDENTIFIER = @"CellView";


@interface JMFCollectionView ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) ModelDummy *model;

@property (nonatomic, strong) NSIndexPath *itemIndexPathCurrent;
@property (nonatomic,strong) NSString *tag;
@property (nonatomic) UICollectionViewCell *cellOld;


@end

@implementation JMFCollectionView

-(id) initWithCollectionView:(UICollectionView *) collectionView                // Vista del collectionview.
                       model:(id)model                                          // ** Modelo (métodos: countItems, countSections, image). **
          setScrollDirection:(UICollectionViewScrollDirection) scrollDirection  // ScrollDirection
                    delegate:(id)aDelegate                                      // Delegado (-> ItemChanged)
                         tag:(NSString *)tag                                    // Etiqueta (title)
        itemIndexPathCurrent:(NSIndexPath *)itemIndexPathCurrent         // Elemento actual.
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        _model = model; //[Model new];
        [self setup:collectionView setScrollDirection:scrollDirection delegate:aDelegate tag:tag itemIndexPathCurrent:itemIndexPathCurrent];
        
    }
    return self;
}







-(void) setup:(UICollectionView *) collectionView                // Vista del collectionview.
setScrollDirection:(UICollectionViewScrollDirection) scrollDirection  // ScrollDirection
    delegate:(id)aDelegate                                      // Delegado (-> ItemChanged)
         tag:(NSString *)tag                                    // Etiqueta (title)
itemIndexPathCurrent:(NSIndexPath *)itemIndexPathCurrent         // Elemento actual.
{
    
    _collectionView = collectionView;
    _itemIndexPathCurrent = itemIndexPathCurrent;
    
    /* ---------------------
     * setup CollectionView
     ----------------------*/
    [self.collectionView registerClass:[JMFCollectionViewCell class] forCellWithReuseIdentifier:CELL_IDENTIFIER];
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([JMFCollectionViewHeader class]) bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                 withReuseIdentifier:NSStringFromClass([JMFCollectionViewHeader class])];
    
    
    /*
     //        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
     UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
     //        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
     //        [collectionView setPagingEnabled:YES];
     
     //        [flowLayout setScrollDirection:scrollDirection];
     flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 20, 10);
     flowLayout.headerReferenceSize = CGSizeMake(self.collectionView.frame.size.width, 10.f);
     //        flowLayout.minimumInteritemSpacing = 50;
     //        flowLayout.minimumLineSpacing = 50;
     [self.collectionView setCollectionViewLayout:flowLayout];
     */
    
    if (scrollDirection == UICollectionViewScrollDirectionVertical) {
        
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
        //            flowLayout.sectionInset = UIEdgeInsetsMake(0, 5, 10, 5);
        flowLayout.headerReferenceSize = CGSizeMake(self.collectionView.frame.size.width, 20.f);
        
        
        [self.collectionView setCollectionViewLayout:flowLayout];
    }
    
    
    
    
    
    /* --------
     * Delegado
     ----------*/
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    __delegate = aDelegate;
    _tag = tag;
    
    
}






- (void)viewDidLoad {
    
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






#pragma mark - UICollectionView Datasource


- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
//    int result;
//    if (self.model != nil)
//        result = [self.model countItems:section];
//    else result = [[self.itemsSeletedDictionary allKeys] count];
//    
//    return result;
    
    int result = [self.model countItems:section];
    return result;
    //    return [self.dataArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    int result = [self.model countSections];
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JMFCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    cell.img.image = [self.model getItemImage:indexPath]; //[UIImage
    
    
    // Selecciona item.
    if (indexPath.row == self.itemIndexPathCurrent.row && indexPath.section == self.itemIndexPathCurrent.section)
        [self checkItem:cell];
    else
        [self uncheckItem:cell];
    
    // coner
    cell.img.layer.cornerRadius = 5;
    cell.img.layer.masksToBounds = YES;
    
    return cell;
}

/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    [self.delegate onClickItemChangedCollectionViewWithTag:self.tag itemIndexPath:indexPath];
//    
//}


- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    
    // Selecciona item.
    [self checkItem:cell];
    
    
    // Deseleciona anterior click
    self.itemIndexPathCurrent = indexPath;
    
    [self._delegate onClickItemChangedCollectionViewWithTag:self.tag itemIndexPath:indexPath];
}

/*
 - (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
 UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
 //    cell.contentView.backgroundColor = nil;
 
 }
 */



#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    JMFCollectionViewCell *cell = [JMFCollectionViewCell new];
    
    // Se ajusta al tamaño de la celda configurada en el XIB'.
    CGSize cellSize = CGSizeMake(cell.bounds.size.width, cell.bounds.size.height);
    
    return cellSize;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

//- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return [self getCellSize:indexPath];  // will be w120xh100 or w190x100
//    // if the width is higher, only one image will be shown in a line
//}



/*
 // Espaciado entre las celdas, las cabeceras y pies de página
 - (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout*)collectionViewLayout
 insetForSectionAtIndex:(NSInteger)section {
 
 return UIEdgeInsetsMake(50, 20, 50, 20);
 }
 */

/*
 -(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
 
 UICollectionReusableView *reusableview = nil;
 
 if (kind == UICollectionElementKindSectionHeader) {
 JMFCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEADER_IDENTIFIER forIndexPath:indexPath];
 NSString *title = [[NSString alloc]initWithFormat:@"Recipe Group #%i", indexPath.section + 1];
 headerView.title.text = title;
 
 reusableview = headerView;
 }
 
 
 return reusableview;
 
 }
 
 //- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
 //
 //}
 */

- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView * view = nil;
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        JMFCollectionViewHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([JMFCollectionViewHeader class]) forIndexPath:indexPath];
        
        header.title.text = [self._delegate onGetSectionNameWithTag:indexPath.section tag:self.tag];
        
        view = header;
    }
    
    return view;
}


#pragma mark - Métodos públicos





/* .................
 *
 *  Selecciona item
 *
 ...................*/
-(void) checkItem:(UICollectionViewCell*) cell {
    
    //    cell.contentView.backgroundColor = [Utils colorWithHexString:COLOR_MadeInMe];
    cell.layer.borderColor = [[Utils colorWithHexString:COLOR_MadeInMe] CGColor];
    cell.layer.borderWidth = 4;
    
    if (self.cellOld && self.cellOld != cell)
        [self uncheckItem:self.cellOld];
    
    self.cellOld = cell;
    
}

-(void) uncheckItem:(UICollectionViewCell*) cell {
    
    //        self.cellOld.contentView.backgroundColor = [UIColor clearColor];
    cell.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.layer.borderWidth = 0;
    
    
}

/*
 -(void) refresh {
 //    [self.collectionView reloadData];
 self.collectionView.backgroundColor = [UIColor redColor];
 //    [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
 [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
 dispatch_async(dispatch_get_main_queue(), ^
 {
 [self.collectionView reloadData];
 
 });
 
 }
 */


//-(NSString *) getTitle {
//    return self.itemTitle;
//    //    return [self.model getShoeTypeTAG];
//}






@end
